# LINMOBapps

## LINux on MOBile Apps

This project is served at [https://linmobapps.frama.io/](https://linmobapps.frama.io/) and has been forked from the great [https://mglapps.frama.io/](https://mglapps.frama.io) list in order to prepare the creation static site generator based system that lists all the same apps. [Click here to read about the current plans](https://linmob.net/linmobapps-additions-changes-march-april-2021/#the-future-linuxphoneapps-org) and make sure to chime in [here regarding audience needs](https://pad.hacc.space/6KZJFltXSHSMCa-4NaBYag?edit) and [there regarding implementation](https://pad.hacc.space/xKq47oRxQqSuhz21z8lnEg?view). More Apps and Colums indicating app availablilty across distributions and status have been added. If you want to help out with the work on LinuxPhoneApps.org head over to [GitHub](https://github.com/linuxphoneapps/linuxphoneapps.org)!

Peter also writes about added apps in his [weekly update](https://linmob.net/categories/weekly-update).

LINux on MOBile Apps (short: LINMOBapps) is a list of (potential) applications for usage on mobile devices running GNU/Linux, having small(er) screens and touch input (e.g. smartphones, tablets, convertibles).

Files:
* [index.html](index.html): Main page hosted at [https://linmobapps.frama.io/](https://linmobapps.frama.io/)
* [complete.csv](complete.csv): Complete list of apps, to maintain remerge-ability to MGLapps, no longer maintained,
* __[apps.csv](apps.csv)__: Main app list (subset of complete.csv), to be edited directly. _Add your apps here!_
* __[apps-to-be-added.md](apps-to-be-added.md)__: Apps waiting to be added.
* [games.html](games.html): Page to display the Game List hosted at [https://linmobapps.frama.io/](https://linmobapps.frama.io/games.html)
* __[games.csv](games.csv)__: Main games list (subset of complete.csv), to be edited directly.
* [archive.html](archive.html): Page to display the Archive List hosted at [https://linmobapps.frama.io/](https://linmobapps.frama.io/archive.html)
* [archive.csv](archive.csv): Retired apps (subset of complete.csv).
* [other games.csv](other apps.csv): Further games which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the main list

Just like its origin [MGLApps](https://mglapps.frama.io), LINux on MOBile Apps is licensed under CC BY-SA 4.0 International: [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/) . For more licensing information, see [LICENSE](LICENSE)

If you want to help, check [CONTRIBUTING.md](CONTRIBUTING.md) or just check the [open Issues](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues) and feel free to open some new ones! 
If you want to work on a better future, head over to [GitHub](https://github.com/linuxphoneapps/linuxphoneapps.org)!

## Removed content / experimental packaging scripts

For a while this repo contained a few PKGBUILD scripts (for building packages on Arch or Manjaro) and an APKBUILD (for postmarketOS) scripts.
These have since been moved to their own repositories:

* [APKBUILDs](https://framagit.org/linmobapps/apkbuilds)
* [PKGBUILDs](https://framagit.org/linmobapps/pkgbuilds)
