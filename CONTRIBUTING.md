# Contributing

Contributions are welcome! Every of your contributions to the content (mainly apps.csv and games.csv) has to be licensed under CC-BY-SA-4.0. 

It is recommended to edit the lists in LibreOffice Calc.

[TBD: Explain CSV import (, as seperator) and efficient editing (highlight B2 and click View > Freeze Rows and Columns")]

For apps.csv, please add apps below existing apps.

## Easy tasks, great for first-time contributions:

### Check if apps are available on Arch, Mobian, postmarketOS or Flathub. You can easily do so by using [Repology](https://repology.org/).
* If you are running another distribution, feel free to add an extra column for it and start filling it!
* Please note: While this is important, a future iteration of this list will likely just include a repology badge, so don't invest all of your time into this!

### Add a new app
* Candidates are listed in [apps-to-be-added.md](apps-to-be-added.md).

### Games to add to the game list (make sure to put it in in alphabetical order)
* AisleRiot (see https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7)
* Taquin (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7)
* Reversi (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/13) 
* Gnubik (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/26)
* Foobillardplus (https://github.com/alrusdi/foobillardplus | http://foobillardplus.sourceforge.net/ | https://sourceforge.net/projects/foobillardplus/ ~ https://pkgs.org/search/?q=foobillardplus; https://pkgs.org/search/?q=foobillardplus)
* and more on [other games.csv](other games.csv).
   
## Further content maintenance tasks:
* ~~Check and remove further apps that are no longer available (e.g. source code gone) by copying them to archive.csv.~~
* ~~Add games or Apps from "other games.csv" or "other apps.csv".~~
* fix typos :D
* check links again
* add Freedesktop [categories](https://specifications.freedesktop.org/menu-spec/latest/apa.html) and [Additional categories](https://specifications.freedesktop.org/menu-spec/latest/apas02.html) to a new field, old categories are supposed to stay. _In progress_
* ~~transition "License" over to [SPDX identifiers](https://spdx.org/licenses/)~~
* add URLs for appstream-files (per project) _almost done, needs to be re-checked_

## Design tasks: 
* ~~Make AppStream Links work.~~
* Improve mobile design generally.
* Implement a way to display screenshots and or app logos.
* Implement a form with the necessary fields for apps.csv generation for easier email submission.
